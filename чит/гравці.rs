use core::fmt;
use std::sync::Mutex;
use deepsize::DeepSizeOf;
use rand::seq::SliceRandom;
use trees::Tree;
use std::io::{stdin};
use lazy_static::lazy_static;

use crate::дошка::{Дошка};
use crate::гра::{Хід};
use crate::дерево_ходів_гри::{ВузолДерева, вДеревоХодів};


#[derive(Clone, Copy)]
#[derive(DeepSizeOf)]
#[derive(PartialEq)]
pub enum ЗнаменоГравця { А, Б }
impl fmt::Display for ЗнаменоГравця {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ЗнаменоГравця::А => write!(f, "А"),
            ЗнаменоГравця::Б => write!(f, "Б"),
        }
    }
}

pub struct ГравецьАбиДе {
    pub знаменоГравця: ЗнаменоГравця,
}

pub struct ГравецьЛюдина {
    pub знаменоГравця: ЗнаменоГравця,
}

pub struct ГравецьНадрозум {
    pub знаменоГравця: ЗнаменоГравця,
}

pub enum Гравець {
    ГравецьАбиДе(ГравецьАбиДе),
    ГравецьЛюдина(ГравецьЛюдина),
    ГравецьНадрозум(ГравецьНадрозум),
}

pub trait ХідГравця {
    fn вХід(&self, дошка: &Дошка, минХоди: &Vec<(Хід, ЗнаменоГравця)>, деревоХодів: &ВузолДерева) -> Option<Хід>;
}

impl ХідГравця for ГравецьАбиДе {
    fn вХід(&self, дошка: &Дошка, минХоди: &Vec<(Хід, ЗнаменоГравця)>, деревоХодів: &ВузолДерева) -> Option<Хід> {
        let вкази = дошка.непустіЯмки(self.знаменоГравця == ЗнаменоГравця::А);
        let можлВказ = вкази.choose(&mut rand::thread_rng());
        if можлВказ.is_some() {
            let вказ = можлВказ.unwrap();
            return Some(Хід { вк: *вказ as i8 })
        } else {
            return None
        }
    }
}

impl ХідГравця for ГравецьЛюдина {
    fn вХід(&self, дошка: &Дошка, минХоди: &Vec<(Хід, ЗнаменоГравця)>, деревоХодів: &ВузолДерева) -> Option<Хід> {
        let mut вхід = String::new();
        stdin().read_line(&mut вхід).expect("error: unable to read user input");
        let mut можлВказ = вхід.trim().parse::<usize>();

        if можлВказ.is_ok() {
            return Some(Хід { вк: (можлВказ.unwrap() - 1) as i8 })
        } else {
            return None
        }
    }
}

impl ХідГравця for ГравецьНадрозум {
    fn вХід(&self, дошка: &Дошка, минХоди: &Vec<(Хід, ЗнаменоГравця)>, деревоХодів: &ВузолДерева) -> Option<Хід> {
        let mut поточнеДерево = Some(деревоХодів);
        for хід in минХоди {
            if поточнеДерево.is_none() { break; };

            let підД = поточнеДерево.unwrap().діти.iter().find(|підД| {
                let хідД = підД.хід;
                return хід.0.вк == хідД.вк;
            });

            поточнеДерево = підД;
        }
        
        if поточнеДерево.is_none() || поточнеДерево.unwrap().діти.len() == 0 {
            println!("дпоооооооооомогаааа");
            return деревоВХід(self.знаменоГравця, &вДеревоХодів(дошка.clone(), self.знаменоГравця, минХоди.last().unwrap().0))
        } else {
            return деревоВХід(self.знаменоГравця, поточнеДерево.unwrap())
        };
    }
}

fn деревоВХід(знаменоГравця: ЗнаменоГравця, поточнеДерево: &ВузолДерева) -> Option<Хід> {
    // поточнеДерево.діти.iter().filter(|д| д.оцінка != core::i8::MIN).for_each(|підд| print!("{},", підд.хід.вк));
    поточнеДерево.діти.iter().for_each(|підд| print!("{},", підд.оцінка));
    let найкХід = if знаменоГравця == ЗнаменоГравця::А {
        поточнеДерево.діти.iter().max_by_key(|підд| підд.оцінка)
    } else {
        поточнеДерево.діти.iter().min_by_key(|підд| підд.оцінка)
    };
    if найкХід.is_none() { return None };

    return Some(найкХід.unwrap().хід);
}