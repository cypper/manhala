use std::{borrow::{Borrow, BorrowMut}, fmt::Display};
use once_cell::sync::Lazy;
use deepsize::DeepSizeOf;

use trees::{tr, Node, Tree};
use crate::гра::{Гра, Хід, Вислід};
use crate::гравці::{Гравець, ГравецьАбиДе, ГравецьЛюдина, ЗнаменоГравця};
use crate::дошка::{Дошка, створитиДошку};

fn tree_to_string( node: &ВузолДерева, глибина: i32 ) -> String {
    if node.діти.len() == 0 {
        format!("{}{}", " ".repeat((глибина*2 - 1) as usize), node.оцінка)
    } else {
        format!( "{}{} (\n{}\n{})\n",
            " ".repeat((глибина*2) as usize),
            node.оцінка,
            // " ".repeat((глибина*2 + 2) as usize),
            node.діти.iter().fold( String::new(),
                |s,c| s + &tree_to_string(c, глибина + 1) + &" " ),
            " ".repeat((глибина*2) as usize),
            )
    }
}

fn всіНастХоди(дерево: &mut ВузолДерева, гра: &mut Гра, знаменоГравця: ЗнаменоГравця, поперПередОцінка: i8, глибина: i8, гол: ЗнаменоГравця) {
    let mut поперОцінка = дерево.оцінка;

    for вказ in гра.дошка.непустіЯмки(знаменоГравця == ЗнаменоГравця::А) {
        let хід = Хід { вк: вказ as i8 };
        let mut підгра = гра.clone();

        let (чиВідбувсяХід, чиДодХід) = підгра.хід(хід, знаменоГравця);
        if !чиВідбувсяХід { panic!("не може походити ааааааа") }

        let настГравець = if чиДодХід {
            знаменоГравця
        } else {
            if знаменоГравця == ЗнаменоГравця::А { ЗнаменоГравця::Б } else { ЗнаменоГравця::А }
        };
        
        let mut підДерево = ВузолДерева {
            оцінка: поперОцінка,
            знаменоГравця: настГравець,
            хід: хід,
            діти: vec![],
        };

        if підгра.дошка.стан().is_some() {
            let оцінка = оцінкаСтану(&підгра.дошка, підДерево.знаменоГравця);
            підДерево.оцінка = оцінка;

            if знаменоГравця == ЗнаменоГравця::А {
                if поперОцінка < оцінка { поперОцінка = оцінка };
            } else {
                if поперОцінка > оцінка { поперОцінка = оцінка };
            };
        } else if глибина >= 13 {
            let оцінка = неповнаОцінкаСтану(&підгра.дошка, підДерево.знаменоГравця);
            підДерево.оцінка = оцінка;

            if знаменоГравця == ЗнаменоГравця::А {
                if поперОцінка < оцінка { поперОцінка = оцінка };
            } else {
                if поперОцінка > оцінка { поперОцінка = оцінка };
            };
        } else {
            всіНастХоди(&mut підДерево, &mut підгра, настГравець, поперОцінка, глибина+1, гол);

            let оцінка = оцінкаДерева(&підДерево);
            підДерево.оцінка = оцінка;

            if знаменоГравця == ЗнаменоГравця::А {
                if поперОцінка < оцінка { поперОцінка = оцінка };
            } else {
                if поперОцінка > оцінка { поперОцінка = оцінка };
            };
        };

        дерево.діти.push(підДерево);

        if знаменоГравця == ЗнаменоГравця::А {
            if поперПередОцінка != core::i8::MIN && поперОцінка > поперПередОцінка { break; };
            // if гол == ЗнаменоГравця::А && поперПередОцінка != core::i8::MIN && поперОцінка > поперПередОцінка { break; };
        } else {
            if поперПередОцінка != core::i8::MIN && поперОцінка < поперПередОцінка { break; };
            // if гол == ЗнаменоГравця::Б && поперПередОцінка != core::i8::MIN && поперОцінка < поперПередОцінка { break; };
        };
    }

    let оцінка = оцінкаДерева(&дерево);
    дерево.оцінка = оцінка;
}

fn оцінкаСтану(дошка: &Дошка, знаменоГравця: ЗнаменоГравця) -> i8 {
    let можлСтан = дошка.стан();

    if можлСтан.is_none() {
        panic!("Оні");
    } else {
        // let стан = можлСтан.unwrap();

        return дошка.очкиА() - дошка.очкиБ();
        // if стан == Вислід::ПЕРЕМОГА_А {
        //     return 20;
        // }
        // else if стан == Вислід::ПЕРЕМОГА_Б {
        //     return 0;
        // } else {
        //     return 10;
        // };
    }
}

fn неповнаОцінкаСтану(дошка: &Дошка, знаменоГравця: ЗнаменоГравця) -> i8 {
    return дошка.неповніОчкиА() - дошка.неповніОчкиБ();
}


fn оцінкаДерева(дерево: &ВузолДерева) -> i8 {
    let можлОцінка = if дерево.знаменоГравця == ЗнаменоГравця::А {
        дерево.діти.iter().max_by_key(|підд| підд.оцінка)
    } else {
        дерево.діти.iter().min_by_key(|підд| підд.оцінка)
    };


    if можлОцінка.is_none() { panic!("Ого") };

    return можлОцінка.unwrap().оцінка
}

// fn оцінкаВГлибину(дерево: &mut Node<ВузолДерева>) {
//     if дерево.has_no_child() {
//         let mut вузол = дерево.data_mut();
//         let оцінка = оцінкаСтану(&вузол.дошка, вузол.знаменоГравця);
//         вузол.оцінка = оцінка;
//     } else {
//         for піддерево in дерево.iter_mut() {
//             оцінкаВГлибину(піддерево.get_mut());
//         }

//         let оцінка = оцінкаДерева(дерево);

//         let вузол = дерево.data_mut();
//         вузол.оцінка = оцінка;
//     }
// }

#[derive(DeepSizeOf)]
pub struct ВузолДерева {
    pub хід: Хід,
    pub знаменоГравця: ЗнаменоГравця,
    pub оцінка: i8,
    pub діти: Vec<ВузолДерева>,
}

pub fn вДеревоХодів(дошка: Дошка, знаменоГравця: ЗнаменоГравця, хід: Хід) -> ВузолДерева {
    let mut гра = Гра { дошка };


    let mut дерево = ВузолДерева { оцінка: core::i8::MIN, знаменоГравця, хід, діти: vec![] };
    всіНастХоди(&mut дерево, &mut гра, знаменоГравця, core::i8::MIN, 0, знаменоГравця);

    // оцінкаВГлибину(&mut дерево.root_mut());

    // print!("ghjk: {}.", tree_to_string(&дерево, 1));
    print!("ввв: {}.\n", дерево.оцінка);

    return дерево;
}

// std::sync::Once + static mut деревоХодів: Tree<ВузолДерева> = вДеревоХодів();